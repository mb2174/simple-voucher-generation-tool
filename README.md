## Simple voucher generation tool

The Python script generates vouchers based on a specification file in CSV format located by default in `./data/` directory.

The test and production variables are defined in `config.ini`.

### Requirements
- gpg encrypted private keys for test and production environments. The paths of the keys should be defined in `config.ini`.

### Installation
```bash
$ git clone git@gitlab.developers.cam.ac.uk:uis/infra/simple-voucher-generation-tool.git
```
### Generate the keys
To generate the [Elliptic Curve](https://tools.ietf.org/html/rfc7518#section-3.4) (EC) public and private keys (the example below is for the `test` environment keys):
```bash
$ cd simple-voucher-generation-tool/keys/
$ openssl ecparam -genkey -name prime256v1 -noout -out ec_private_test.pem
$ openssl ec -in ec_private.pem -pubout -out ec_public_test.pem
```
Then, encrypt the keys:
```bash
$ gpg --encrypt --recipient "Your User ID" ec_private_test.pem
$ gpg --encrypt --recipient "Your User ID" ec_public_test.pem
```

### Run the script
Given the following specification:
```bash
$ cat ./data/vouchers_spec.csv
# School or College | Group IDs | Group names | AD domain | CRSID | Size (TB) | Duration (Yrs) | Value (GBP) | Invalid before | Expiry date | Account | Comment
School; ENG; Department of Engineering; blue.cam.ac.uk; abc12; 4; 1; 600; 17/04/2020 00:00:00; 17/04/2021 00:00:00; TBD; Test
```

To generate the vouchers:
```bash
$ python generate-vouchers.py 
Writing vouchers in ./data/vouchers.json
Writing vouchers in ./data/vouchers.csv
```

The generated vouchers are stored by default in `./data/vouchers.json`.
```json
{
    "021b4c45e40347c5bdef84d0eb1a0395": {
        "School or College": "School",
        "Group IDs": "ABC",
        "Group names": "Department ABC",
        "AD domain": "ad.domain",
        "CRSID": "abc12",
        "Size (TB)": "4",
        "Duration (Yrs)": "1",
        "Value (GBP)": "600",
        "Invalid before": "17/04/2020 00:00:00",
        "Expiry date": "17/04/2021 00:00:00",
        "Voucher ID": "021b4c45e40347c5bdef84d0eb1a0395",
        "Voucher": "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImlzcyI6Imlmcy1saXZlLWVpYmFoN2hhaDgiLCJhdWQiOiJzc2d3In0.eyJqdGkiOiIwMjFiNGM0NWU0MDM0N2M1YmRlZjg0ZDBlYjFhMDM5NSIsImlzcyI6Imlmcy1saXZlLWVpYmFoN2hhaDgiLCJhdWQiOiJzc2d3IiwidmFsIjoiNjAwIiwiY3JzaWQiOiJhYmMxMiIsImlhdCI6MTU4NzA3ODAwMCwibmJmIjoxNTg3MDc4MDAwLCJleHAiOjE2MTg2MTQwMDB9.xbYv5OzjfYvmB5in8TVZITanLIP4ni923n4kMNiHIutzzJagBp758lhckMGECskN8U2fT4ElEjTVW9yrw0bsXA",
        "Account": "TBD",
        "Comment": "Test"
    }
}
```

You can verify the voucher (`Voucher` key in the json object above) on the [SSGW Voucher Debugger](https://rjw57.github.io/voucher-tool/).
